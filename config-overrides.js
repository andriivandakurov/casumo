
const { override, addBabelPlugin } = require("customize-cra");

function myOverrides(config) {
  config.module.rules.push({
    test: /\.worker\.js$/,
    use: { loader: 'worker-loader' }
  });

  return config
}

module.exports = override(  
  myOverrides,
  addBabelPlugin('@babel/plugin-transform-typescript')
);