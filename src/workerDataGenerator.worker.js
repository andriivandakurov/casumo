import { getData } from './utils';

/* eslint-disable-next-line no-restricted-globals */
self.addEventListener('message', (e) => {  
  const objectCount = e.data; 

  /* eslint-disable-next-line no-restricted-globals */
  self.postMessage(getData(objectCount));
});

/* eslint-disable-next-line no-restricted-globals */
export default self;