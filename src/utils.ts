import { SortDirectionType } from 'react-virtualized';
import Chance from 'chance';
import { IListItem } from './types';

const chance = new Chance(); 
export const getData = (objectCount = 1) => [...Array(objectCount)].map(() => ({
  name: chance.word(),
  author: chance.name(),
  gender: chance.gender(),
  genre: chance.word(),
  publishDate: chance.date({ string: true })
}));

export const sortArray = (arr: IListItem[], sortBy: string, direction: SortDirectionType) => arr.sort((a: any, b: any) => {
  const d = direction === 'ASC' ? 1 : -1;
  if (a[sortBy] > b[sortBy]) return 1 * d;
  if (a[sortBy] < b[sortBy]) return -1 * d;
  return 0;
});
