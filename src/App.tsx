import React, { useState, useCallback, useEffect } from 'react';
import { Column, Table, SortDirectionType } from 'react-virtualized';
import Loader from 'react-loader';
import { ISortProps } from './types';
import { sortArray, getData } from './utils';
import dataWorker from './workerDataGenerator.worker';

import 'react-virtualized/styles.css';
import './App.css';

const DataWorker: any = dataWorker;
const dataProvider = new DataWorker();

const initialState: ISortProps = {
  sortBy: 'author',
  sortDirection: 'ASC'
}

const App = () => {
  const sortedData = useCallback(() => sortArray(getData(5), initialState.sortBy, initialState.sortDirection), []);
  const [sortBy, setSortBy] = useState(initialState.sortBy);
  const [sortDirection, setSortDirection] = useState(initialState.sortDirection);
  const [isLoading, setLoading] = useState(false);
  const [list, setList] = useState(sortedData);

  useEffect(() => {
    dataProvider.addEventListener('message', (event: any) => {
      setLoading(false);
      setList(event.data);
    });
  }, [])

  const _sort = ({ sortBy, sortDirection }: ISortProps) => {
    setList(sortArray(list, sortBy, sortDirection))
    setSortBy(sortBy);
    setSortDirection(sortDirection);
  }

  const workerGenerateData = (recordsCount: number) => {
    const { Worker } = window as any;

    if (Worker) {
      setLoading(true);
      dataProvider.postMessage(recordsCount);
    }
  }

  const generate1m = useCallback(() => workerGenerateData(1000000), []);
  const generate10 = useCallback(() => workerGenerateData(10), []);
  const generate100 = useCallback(() => workerGenerateData(100), []);

  return (
    <div className="app">
      <div className="app-header">
        <button onClick={generate10} className="button-success pure-button"> Generate 10 records</button>
        <button onClick={generate100} className="button-warning pure-button"> Generate 100 records</button>
        <button onClick={generate1m} className="button-error pure-button"> Generate 1 million records</button>

        <p>Generation of 1 million records can take some time so please wait.</p>
      </div>

      <Loader loaded={!isLoading}>
        <Table
          width={700}
          height={500}
          headerHeight={20}
          sort={_sort}
          sortBy={sortBy}
          sortDirection={sortDirection as SortDirectionType}
          rowHeight={30}
          rowCount={list.length}
          rowGetter={({ index }) => list[index]}
        >
          <Column label="Name" dataKey="name" width={200} />
          <Column label="Author" cellDataGetter={({ rowData }) => `${rowData.author} (${rowData.gender})`} dataKey="author" width={400} />
          <Column label="Genre" dataKey="genre" width={200} />
          <Column label="Publish date" dataKey="publishDate" width={300} />
        </Table>
      </Loader>
    </div>
  );
}

export default App;
