import { SortDirectionType } from 'react-virtualized';

export interface IListItem {
  name: string;
  author: string;
  genre: string;
  publishDate: Date | string;
}

export interface ISortProps {
  sortBy: string;
  sortDirection: SortDirectionType
}